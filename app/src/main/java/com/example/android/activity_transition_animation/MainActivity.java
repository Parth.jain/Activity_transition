package com.example.android.activity_transition_animation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnSlideUp,btnSlidedown,btnSlideLeft,btnSlideRight;

        btnSlideUp=findViewById(R.id.slideUPBtn);
        btnSlidedown=findViewById(R.id.slideDownBtn);
        btnSlideLeft=findViewById(R.id.slideLeftBtn);
        btnSlideRight=findViewById(R.id.slideRightBtn);

        btnSlideLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,SecondLeftActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                //finish();
            }
        });
        btnSlideRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             startActivity(new Intent(MainActivity.this,SecondRightActivity.class));
             overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
             //finish();
            }
        });

        btnSlideUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,SecondUpActivity.class));
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_down);
            }
        });


        btnSlidedown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,SecondDownActivity.class));
                overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_up);
            }
        });


    }
}
