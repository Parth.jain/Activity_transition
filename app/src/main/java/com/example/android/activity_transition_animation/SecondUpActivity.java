package com.example.android.activity_transition_animation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SecondUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_up);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_up);
    }
}
