package com.example.android.activity_transition_animation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SecondDownActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_down);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_down);
    }
}
